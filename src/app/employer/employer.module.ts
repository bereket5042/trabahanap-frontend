import { EmployerComponent } from './employer.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployerRoutingModule } from './employer-routing.module';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [
    EmployerComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    EmployerRoutingModule
  ]
})
export class EmployerModule { }
