import { ApplicantComponent } from './applicant.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ApplicantRoutingModule } from './applicant-routing.module';
import { HomeComponent } from './components/home/home.component';


@NgModule({
  declarations: [
    ApplicantComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    ApplicantRoutingModule
  ]
})
export class ApplicantModule { }
