
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Custom Modules
import { AdminModule } from './admin/admin.module';
import { EmployerModule } from './employer/employer.module';
import { AnonymousModule } from './anonymous/anonymous.module';
import { ApplicantModule } from './applicant/applicant.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AnonymousModule,
    ApplicantModule,
    EmployerModule,
    AdminModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
