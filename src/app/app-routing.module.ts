import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'anonymous', pathMatch: 'full'},
  {
    path: 'anonymous',
    loadChildren: () => import('./anonymous/anonymous.module').then(mod => mod.AnonymousModule)
  },
  {
    path: 'applicant',
    loadChildren: () => import('./applicant/applicant.module').then(mod => mod.ApplicantModule)
  },
  {
    path: 'employer',
    loadChildren: () => import('./employer/employer.module').then(mod => mod.EmployerModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(mod => mod.AdminModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
